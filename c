#!/bin/bash

DIR=$(pwd)
TAB=$(printf '\t')
VERSION=1.0.0

new () {
	cd "$DIR" &&
	mkdir "$1" &&
	cd "$1" &&
	mkdir src &&
	touch .gitignore Makefile README LICENSE src/main.c &&
	wget -qO LICENSE https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt 2> /dev/null &&	
	generate_Makefile &&
	generate_main_src &&
	git_initialization
}

generate_Makefile () {
	cat > Makefile <<- END
		BUILD_DIR := ./build
		SRC_DIRS := ./src
		TARGET_EXEC := \${BUILD_DIR}/main
		
		\${TARGET_EXEC}: \${SRC_DIRS}/main.c
			${TAB}mkdir -p \${BUILD_DIR}
			${TAB}\$(CC) \$? -o \$@

		.PHONY: clean
		clean:
			${TAB}rm -r \${BUILD_DIR}
	END
}

generate_main_src () {
	cat > src/main.c <<- END
		#include <stdio.h>
		#include <stdlib.h>
		#include <string.h>

		int
		main()
		{
			${TAB}printf("Hello world!\n");

			${TAB}return 0;
		}		
	END
}

git_initialization () {
	if [[ -e `which git 2> /dev/null` ]]; then
		git init -q
	fi
	echo "build/" > .gitignore
}

usage () {
	echo
	echo c "${VERSION}"
	echo Simple C project generator script
	echo
	echo USAGE:
	echo -e "\t$(basename $0)" [OPTIONS] \<SUBCOMMAND\> \<PROJECT_NAME\>
	echo
	echo DESCRIPTION:
	echo -e "\tCreates a new C project in a given directory with git"
    echo -e "\tinitialized. The following files and directories are created:"
    echo
    echo -e "\t- README"
    echo -e "\t- LICENSE"
    echo -e "\t- Makefile"
    echo -e "\t- src/"
	echo
	echo -e "\tAfter creating the source files, simply run 'make' in the project root"
    echo -e "\tdirectory in order for the executable file to be created inside"
    echo -e "\tthe build directory"
	echo
	echo OPTIONS:
	echo -e "\t-v, --verbose\tenable verbose output"
	echo -e "\t-h, --help\tprints help information"
	echo -e "\t-V, --version\tprints version information"
	echo
	echo SUBCOMMAND:
	echo -e "\tnew\t\tcreates new project"
	echo -e "\thelp\t\tprints help information"

}	

shopt -s extglob	# enable extended pattern matching in case statements

case "$1" in
	new)
		if [[ "$2" == "" ]]; then
			echo No project name is provided
			exit 1
		else
			new "$2"
			echo New project "$2" was created
		fi
		;;
	?\(-\)h | ?\(--\)help)
		usage | less
		;;
	-V | --version)
		echo "${VERSION}"
		;;
	*)
		usage | less
		exit 1
		;;
esac

shopt -u extglob
